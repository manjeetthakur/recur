import { Component, OnInit, OnChanges, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { BsModalComponent } from 'ng2-bs3-modal';
import { RRule } from 'rrule';
import {
  getDay,
  getDate,  
} from 'date-fns';

@Component({
  selector: 'app-recur',
  templateUrl: './recur.component.html',
  styleUrls: ['./recur.component.scss']
})
export class RecurComponent implements OnInit, OnChanges {

  endsOptions: any = [{label:'Never', value: 'never' }, {label:'On', value: 'on' }, {label:'After', value: 'after' }];

  /**
   * recurring 
   */
  @Input() recurSettings: any = null;

  /**
   * if frequency is month, show monthly repeat message..
   */
  monthlyRepeatMessage: string = '';

  /**
   * recur modal for custom settings
   */
  @ViewChild('recurModal')
  recurModal: BsModalComponent;

  /**
   * on date change, prepare the recurring options
   */
  @Input() date = new Date();

  /**
   * emit when recur option is selected...
   */
  @Output() recurRule = new EventEmitter<any>();

  /**
   * weekdays array required as per rrule
   */
  byWeekDays = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'];

  /**
   * frequency array
   */
  frequency: any = [
    {id: 1, value:'day'},
    {id: 2, value:'week'},
    {id: 3, value: 'month'},
    {id: 4, value: 'year'}
  ]

  /**
   * rrule frequency
   */
  rruleFrequency = {
     1: 'DAILY',
     2: 'WEEKLY',
     3: 'MONTHLY',
     4: 'YEARLY'
  };


  /**
   * set initial options for recurring array
   */
  recurring: any  = [];

  /**
   * custom recurring model
   */
  recurModel: any = {
    recurring: this.recurSettings,
    interval: 1,
    freq: 1,
    ends: 'never'
  };

  /**
   * week days when frequency is set weekly
   */
  weekDays: any = {};


  /**
   * set status of repeat_on 
   */
  repeatOn: boolean = false;


  /**
   * "ends" field validation
   */
  endsField: string = 'never';
  
  /**
   * set true if event is repeated monthly
   */
  repeatOnMonthly: boolean = false;

  constructor() { }

  ngOnInit() {    
  }

  ngOnChanges( changes: any ) {

    if( 'date' in changes ) {

      this.date = new Date(this.date);

      this.createRecurringOptions();
    } 
    
    if ( 'recurSettings' in changes ) { 

      this.recurModel.recurring = this.recurSettings;

      if( this.recurSettings !=  null && this.recurSettings !=  'null' ) {
        let rrule = RRule.fromString(this.recurSettings);
        let rrule_txt = rrule.toText();

        this.recurring.unshift({
          id: this.recurSettings,
          value: rrule_txt
        }); 
      }

      this.removeDuplicateRecurring();
    }
  }

  removeDuplicateRecurring() {

    let recur_array = [];

    let recur_object = {};

    this.recurring.forEach( recur => {

      if ( recur.value in recur_object ) {
      } else {
        recur_object[recur.value] = recur;

        recur_array.push(recur);  
      }
    });

    this.recurring = recur_array;
  }


  /**
   * prepare recurring options
   */
  createRecurringOptions() {
    
    let recurring  = [];

    recurring.push({
      id: null,
      value: 'Does not repeat'
    });
    
    let week_day = getDay( this.date );

    let month_day = getDate( this.date )
    
    // Daily...
    let rrule_daily = new RRule({
      dtstart: this.date,
      freq: RRule.DAILY      
    });

    recurring.push({
      id: rrule_daily.toString(),
      value: rrule_daily.toText()
    });

    // every week on week day..
    let rrule_weekly =	new RRule({
      dtstart: this.date,
      freq: RRule.WEEKLY,      
      byweekday: RRule[this.byWeekDays[week_day]]
    });

    recurring.push({
      id: rrule_weekly.toString(),
      value: rrule_weekly.toText()
    });    

    // every month on month day selected
    let rrule_monthly =	new RRule({
      dtstart: this.date,
      freq: RRule.MONTHLY,      
      bymonthday: [month_day]
    });

    recurring.push({
      id: rrule_monthly.toString(),
      value: rrule_monthly.toText()
    });

    // every year
    let rrule_yearly =	new RRule({
      dtstart: this.date,
      freq: RRule.YEARLY      
    });

    recurring.push({
      id: rrule_yearly.toString(),
      value: rrule_yearly.toText()
    });

    // every weekday
    let rrule_weekday =	new RRule({
      dtstart: this.date,
      freq: RRule.WEEKLY,      
      byweekday: [RRule.MO, RRule.TU, RRule.WE, RRule.TH, RRule.FR]
    });

    recurring.push({
      id: rrule_weekday.toString(),
      value: rrule_weekday.toText()
    });

    // custom recurring option
    recurring.push({
      id: 2,
      value: 'Custom...'
    });

    this.recurring = recurring;
    	
    /* let custom = new RRule({
      freq: RRule.MONTHLY,
      byweekday: [RRule.FR.nth(4)]  
    }); */
  }

  /**
   * return selected week days...
   */
  selectedWeekDays() {

    let week_days = [];

    for(let i in this.weekDays ) {
      if(this.weekDays[i])
      week_days.push( RRule[i]);
    }

    return week_days;
  }

  /**
   * 
   * @param frequency frequency which is selected... day|week|month|year
   */
  onSelectingFrequency(frequency: number) {    
    this.repeatOn = (frequency == 2)?true:false;      

    if( frequency == 3 ) {
      this.repeatOnMonthly = true;
      this.monthlyRepeatMessage = `Monthly on day ${getDate( this.date )}`;
    }else {
      this.repeatOnMonthly = false;
    }
  }
  
  keydown = () => false;

  /**
   * This function sets the weekdays in object with their status..
   * @param status - whether weekday checked or unchecked
   * @param week_day  - which weekday checked/unchecked
   */
  onWeeklyRepeat(status: boolean, week_day: string) {    
    this.weekDays[week_day] = status;
  }

  onSelectingRecurring() {
    if(this.recurModel.recurring == 2 ) {
      this.openModal();
    }else{
      this.recurRule.emit(this.recurModel.recurring);
    }    
  }
  
  /*
  * Open recurModal
  */
  openModal() {
    this.recurModel = {
      recurring: 1,
      interval: 1,
      freq: 1,
      ends: 'never',
      end_date: this.date,
      occurrences: 1
    };
    this.recurModal.open();
  }

  /*
  * recurModal dismissed...
  */
  recurModalDismissed() {
    this.recurModel.recurring = this.recurSettings;      
  }

  /*
  * This function is called when 'Done' button is clicked on the modal..
  * modal closed.. create the recur object based on selections...
  */
  recurModalClosed() {

    let rrule: any = {};

    rrule.dtstart = this.date;
    rrule.freq =  RRule[this.rruleFrequency[ this.recurModel.freq ]];
    
    // set interval if it is greater than 1
    if( this.recurModel.interval > 1 )
      rrule.interval = this.recurModel.interval;

    // if frequency is weekly, check for weekdays selected...
    if( this.recurModel.freq == 2 ) {
      let week_days = this.selectedWeekDays();     

      if( week_days.length > 0 ) {
        rrule.byweekday = week_days;
      }
    }

    // if event ends on particular date.. set the same...
    if( (this.recurModel.ends == 'on') && (typeof this.recurModel.end_date !== 'undefined') ) {
      rrule.until = new Date(this.recurModel.end_date );
    }

    // if event is set to end after some occurances...set the same..
    if( (this.recurModel.ends == 'after') && (this.recurModel.occurrences > 0) ) {
      rrule.count = this.recurModel.occurrences;
    }

    let custom_rule = new RRule(rrule);   
    
    let custom_rule_obj = {
      id: custom_rule.toString(),
      value: custom_rule.toText()
    };

    let is_found = this.recurring.filter( iRule => {
      return iRule.value == custom_rule_obj.value;    
    });

    if( is_found.length == 0 ) {
      this.recurring.unshift(custom_rule_obj);
      this.recurModel.recurring = custom_rule_obj.id;
      this.recurRule.emit(this.recurModel.recurring);
    }else{       
      this.recurModel.recurring = is_found[0].id;
      this.recurRule.emit(this.recurModel.recurring);
    }

    this.removeDuplicateRecurring();
  }

  /**
   * set singular/plural forms based on interval selected.
   */
  setFrequency() {
    if( this.recurModel.interval > 1 ) {
      this.frequency = [
        {id: 1, value:'days'},
        {id: 2, value:'weeks'},
        {id: 3, value: 'months'},
        {id: 4, value: 'years'}
      ]
    }else {
      this.frequency = [
        {id: 1, value:'day'},
        {id: 2, value:'week'},
        {id: 3, value: 'month'},
        {id: 4, value: 'year'}
      ]
    }
  }
}
