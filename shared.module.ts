import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsModalModule } from 'ng2-bs3-modal';

import { RecurComponent } from './recur/recur.component';

// import material components
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  imports: [
    CommonModule,    
    FormsModule,        
    ReactiveFormsModule,
    BsModalModule,
    MatDatepickerModule,
    MatIconModule,
    MatRadioModule,
    MatInputModule,
    MatFormFieldModule
  ],
  declarations: [        
    RecurComponent
  ],  
  exports: [        
    RecurComponent
  ]
})
export class SharedModule {}